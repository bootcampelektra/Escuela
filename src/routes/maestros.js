const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/todosMaestros", (resquest, response) => {
  mysqlConnection.query("SELECT b.idMaestro, a.nombre, a.edad, a.apellido, a.telefono,  a.correo, b.cedula, b.gradoAcademico FROM Maestros as b  INNER JOIN Personas as a  ON b.idMaestro = a.idPersona;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/soloMaestro/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT b.idMaestro, a.nombre, a.edad, a.apellido, a.telefono,  a.correo, b.cedula, b.gradoAcademico FROM Maestros as b  INNER JOIN Personas as a  ON b.idMaestro = a.idPersona WHERE b.idMaestro = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/guardarMaestro", async (request, response) => {
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const cedula = request.body.cedula;
  const grado = request.body.grado;
  

  mysqlConnection.query(
    "INSERT INTO Personas (nombre, apellido ,edad, telefono, correo) VALUES (?,?,?,?,?);",
    [nombre, apellido, edad, telefono, correo ],
    (err, rows, fields) => {
      if (!err) {
        const id = rows.insertId;
      

        mysqlConnection.query(
        "INSERT INTO Maestros (idMaestro, cedula, gradoAcademico) VALUES (?,?,?);",
        [id ,cedula, grado],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Maestro agregado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/modificarMaestro/:id", (request, response) => {
  const id = request.params.id;
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const cedula = request.body.cedula;
  const grado = request.body.grado;
  

  mysqlConnection.query(
    "UPDATE Personas SET nombre = ?, apellido = ? ,edad = ?, telefono = ?, correo = ? WHERE idPersona = ?;",
    [nombre, apellido, edad, telefono, correo, id ],
    (err, rows, fields) => {
      if (!err) {
      
      

        mysqlConnection.query(
        "UPDATE Maestros SET  cedula = ?, gradoAcademico = ? WHERE idMaestro = ?;",
        [cedula, grado, id],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Maestro modificado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );



});


//DELETE

router.delete("/api/eliminarMaestro/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM Maestros WHERE idMaestro = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Maestro Eliminado"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
