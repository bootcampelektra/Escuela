const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/todasMaterias", (resquest, response) => {
  mysqlConnection.query("SELECT * FROM Materias;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/solaMateria/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT * FROM Materias WHERE idMateria = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/guardarMateria", async (request, response) => {
  const nombre = request.body.nombre;


  mysqlConnection.query(
    "INSERT INTO Materias (nombre) VALUES (?);",
    [nombre],
    (err, rows, fields) => {
      if (!err) {
        response.json({
            msg: "Materia registrada correctamente"
        });
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/modificarMateria/:id", (request, response) => {
    const id = request.params.id;
    const nombre = request.body.nombre;
    
  
    mysqlConnection.query(
      "UPDATE Materias SET nombre = ?  WHERE idMateria = ?",
      [nombre, id],
      (err, rows, fields) => {
        if (!err) {
          response.json({
            msg: "Materia agregada correctamente"
          });
        } else {
          response.json(err);
        }
      }
    );


});


//DELETE

router.delete("/api/eliminarMateria/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM Materias WHERE idMateria = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Materia eliminada correctamente"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
