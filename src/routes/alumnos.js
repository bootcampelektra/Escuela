const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/todosAlumnos", (resquest, response) => {
  mysqlConnection.query("SELECT b.idAlumno, a.nombre, a.edad, a.apellido, a.telefono, a.correo,  b.numControl FROM Alumnos as b  INNER JOIN Personas as a  ON b.idAlumno = a.idPersona;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/soloAlumno/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT b.idAlumno, a.nombre, a.edad, a.apellido, a.telefono, a.correo, b.numControl FROM Alumnos as b  INNER JOIN Personas as a  ON b.idAlumno = a.idPersona WHERE b.idAlumno = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/guardarAlumno", async (request, response) => {
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const semestre = request.body.semestre;
  const numControl = request.body.numControl;
  

  mysqlConnection.query(
    "INSERT INTO Personas (nombre, apellido ,edad, telefono, correo) VALUES (?,?,?,?,?);",
    [nombre, apellido, edad, telefono, correo ],
    (err, rows, fields) => {
      if (!err) {
        const id = rows.insertId;
      

        mysqlConnection.query(
        "INSERT INTO Alumnos (idAlumno, semestre, numControl) VALUES (?,?,?);",
        [id ,semestre, numControl],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Alumno agregado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/modificarAlumno/:id", (request, response) => {
  const id = request.params.id;
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const semestre = request.body.semestre;
  const numControl = request.body.numControl;
  

  mysqlConnection.query(
    "UPDATE Personas SET nombre = ?, apellido = ? ,edad = ?, telefono = ?, correo = ? WHERE idPersona = ?;",
    [nombre, apellido, edad, telefono, correo, id ],
    (err, rows, fields) => {
      if (!err) {
        
      

        mysqlConnection.query(
        "UPDATE Alumnos SET  semestre = ?, numControl = ? WHERE idAlumno = ?;",
        [semestre, numControl, id],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Alumno modificado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );



});


//DELETE

router.delete("/api/eliminarAlumno/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM Personas WHERE idPersona = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Alumno Eliminado"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
