const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/todosAdministrativos", (resquest, response) => {
  mysqlConnection.query("SELECT b.idAdmiistrativo, a.nombre, a.apellido, a.telefono,  a.edad, a.correo, b.puesto, b.area FROM Administrativo as b  INNER JOIN Personas as a  ON b.idAdmiistrativo = a.idPersona;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/soloAdministrativo/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT b.idAdmiistrativo, a.nombre, a.apellido,  a.edad, a.telefono,  a.correo, b.puesto, b.area FROM Administrativo as b  INNER JOIN Personas as a  ON b.idAdmiistrativo = a.idPersona WHERE b.idAdmiistrativo = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/guardarAdministrativo", async (request, response) => {
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const puesto = request.body.puesto;
  const area = request.body.area;
  

  mysqlConnection.query(
    "INSERT INTO Personas (nombre, apellido ,edad, telefono, correo) VALUES (?,?,?,?,?);",
    [nombre, apellido, edad, telefono, correo ],
    (err, rows, fields) => {
      if (!err) {
        const id = rows.insertId;
      

        mysqlConnection.query(
        "INSERT INTO Administrativo (idAdmiistrativo, puesto, area) VALUES (?,?,?);",
        [id ,puesto , area],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Administrativo agregado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/modificarAdministrativo/:id", (request, response) => {
  const id = request.params.id;
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const apellido = request.body.apellido;
  const telefono = request.body.telefono;
  const correo = request.body.correo;
  const puesto = request.body.puesto;
  const area = request.body.area;

  

  mysqlConnection.query(
    "UPDATE Personas SET nombre = ?, apellido = ? ,edad = ?, telefono = ?, correo = ? WHERE idPersona = ?;",
    [nombre, apellido, edad, telefono, correo, id ],
    (err, rows, fields) => {
      if (!err) {
      

        mysqlConnection.query(
        "UPDATE Administrativo SET  puesto = ?, area = ? WHERE idAdmiistrativo = ?;",
        [puesto, area , id],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Administrativo modificado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );



});


//DELETE

router.delete("/api/eliminarAdministrativo/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM Administrativo WHERE idAdmiistrativo = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Administrativo Eliminado"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
