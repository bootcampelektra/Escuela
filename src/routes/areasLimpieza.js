const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/todasAreasL", (resquest, response) => {
  mysqlConnection.query("SELECT * FROM AreasLimpieza;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/solaAreaL/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT * FROM AreasLimpieza WHERE idAreaLimpieza = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/guardarAreaL", async (request, response) => {
  const nombre = request.body.nombre;
  const ubicacion = request.body.ubicacion;


  mysqlConnection.query(
    "INSERT INTO AreasLimpieza (nombre,ubicacion) VALUES (?,?);",
    [nombre, ubicacion],
    (err, rows, fields) => {
      if (!err) {
        response.json({
            msg: "Area de limpieza registrada correctamente"
        });
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/modificarAreaL/:id", (request, response) => {
    const id = request.params.id;
    const nombre = request.body.nombre;
    const ubicacion = request.body.ubicacion;
    
  
    mysqlConnection.query(
      "UPDATE AreasLimpieza SET nombre = ? , ubicacion = ? WHERE idAreaLimpieza = ?",
      [nombre, ubicacion ,id],
      (err, rows, fields) => {
        if (!err) {
          response.json({
            msg: "Area de limpieza modificada correctamente"
          });
        } else {
          response.json(err);
        }
      }
    );


});


//DELETE

router.delete("/api/eliminarAreaL/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM AreasLimpieza WHERE idAreaLimpieza = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Area de limpieza eliminada correctamente"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
